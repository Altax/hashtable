﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HashTable
{
    public class Node<TValue>
    {
        public string key;
        public TValue value;
        public Node<TValue> next;

        public Node(string key, TValue value, Node<TValue> next)
        {
            this.key = key;
            this.value = value;
            this.next = next;
        }
        public override string ToString()
        {
            return $"Key:{key} Value:{value} \n" + next?.ToString();
        }
        public void RemoveNextByKey(string key)
        {
            if (next == null)
                return;

            if (next.key.Equals(key))
            {
                next = next.next;
            }

            next.RemoveNextByKey(key);
        }
    }
}
