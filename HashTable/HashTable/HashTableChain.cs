﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace HashTable
{
    public class HashTableChain<TValue>
    {
        private Node<TValue>[] array;

        public HashTableChain(int size)
        {
            this.array = new Node<TValue>[size];
        }
        private int GetHash(string key)
        {
            MD5 md5Hasher = MD5.Create();
            var hashed = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(key.ToString()));
            var value = BitConverter.ToInt32(hashed, 0);
            var hash = Math.Abs(value) % array.Length;
            return hash;
        }
        public void Insert(string key, TValue value)
        {

            int h = GetHash(key);
            var lastnode = array[h];
            if (array[h] != null)
            {
                while (lastnode.next != null)
                {
                    lastnode = lastnode.next;
                }
                lastnode.next = new Node<TValue>(key, value, null);
            }
            else
            {
                array[h] = new Node<TValue>(key, value, null);
            }
        }

        public void Show()
        {
            var t = array;
            foreach (var item in t)
            {
                if (item != null)
                Console.WriteLine(item);
            }
        }

        public string Search(string key)
        {
            int h = GetHash(key);
            
            Node<TValue> lastnode = array[h];
            var res = lastnode.value;
            if (lastnode != null)
            {

                while ((lastnode != null))
                    if (lastnode.key.Equals(key))
                    {
                        res = lastnode.value;
                        Console.WriteLine("Your element " + res);
                        lastnode = lastnode.next;
                    }
                    else
                    {
                        lastnode = lastnode.next;
                    }
                return res.ToString();
            }
            else
            {
                Console.WriteLine("This element does not exist");
                return null;
            }
        }


        public void Delete(string key)
        {
            int h = GetHash(key);
            if (array[h] == null)
                return;
            else
            {
                array[h].RemoveNextByKey(key);
                if (array[h].key.Equals(key))
                    array[h] = array[h].next;
            }
        }
        

    }
}
