﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace HashTable
{
    public class HashTableDouble<TValue>
    {
        private NodeDoubleHash<TValue>[] array;

        public HashTableDouble(int size)
        {
            this.array = new NodeDoubleHash<TValue>[size];
        }
        private int GetHash(int key)
        {
            var hash = key % 5 - 1;
            return hash;
        }
        
        public void DoubleHashingInsert(string key, TValue value)
        {
            int i = 0;
            int hashVal = hash1(key);
            int stepSize = hash2(key);

            while (array[hashVal] != null && array[hashVal].key.Equals(-1))
            {
                hashVal += stepSize;
                hashVal %= array.Length;
            }
            array[hashVal] = new NodeDoubleHash<TValue>(key, value);
        }

        public NodeDoubleHash<TValue> DoubleHashingSearch(string key)
        {
            int hashVal = hash1(key);
            int stepSize = hash2(key);

            while (array[hashVal] != null)
            {
                if (array[hashVal].key.Equals(key))
                {
                    Console.WriteLine("Your element " + array[hashVal].value);
                    return array[hashVal];

                }
                hashVal += stepSize;
                hashVal %= array.Length;
            }
            return null;
        }

        public void Show()
        {
            var t = array;
            foreach (var item in t)
            {
                if (item != null)
                    Console.WriteLine(item);
            }
        }

        public NodeDoubleHash<TValue> DoubleHashingDelete(string key)
        {
            int hashVal = hash1(key);
            int stepSize = hash2(key);
            while (array[hashVal] != null)
            {
                if (array[hashVal].key.Equals(key))
                {
                    NodeDoubleHash<TValue> temp = array[hashVal];
                    array[hashVal] = null;
                    return temp;
                }
                hashVal += stepSize;
                hashVal %= array.Length;
            }
            return null;
        }
        private int hash1(string key)
        {
            MD5 md5Hasher = MD5.Create();
            var hashed = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(key.ToString()));
            var value = BitConverter.ToInt32(hashed, 0);
            return Math.Abs(value) % array.Length;
        }
        private int hash2(string key)
        {
            MD5 md5Hasher = MD5.Create();
            var hashed = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(key.ToString()));
            var value = BitConverter.ToInt32(hashed, 0);
            return 5 - Math.Abs(value) % 5;
        }
    }
}

