﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HashTable
{
    public class NodeDoubleHash<TValue>
    {
        public string key;
        public TValue value;
        public Node<TValue> next;

        public NodeDoubleHash(string key, TValue value)
        {
            this.key = key;
            this.value = value;
        }
        public override string ToString()
        {
            return $"Key:{key} Value:{value} \n" + next?.ToString();
        }
    }
}
