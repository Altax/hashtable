﻿using System;
using System.Collections.Generic;

namespace HashTable
{
    class Program
    {
        static void Main(string[] args)
        {
            int tablesize = 13;
            HashTableChain<string> hashTable = new HashTableChain<string>(tablesize);

            hashTable.Insert("234", "354");
            hashTable.Insert("134", "352");
            hashTable.Insert("133", "133");
            hashTable.Insert("456", "456");
            hashTable.Insert("123", "123");
            hashTable.Insert("321", "321");
            hashTable.Insert("327", "327");
            hashTable.Insert("322", "322");
            hashTable.Show();
            hashTable.Delete("234");
            hashTable.Search("133");
            hashTable.Show();
            Console.WriteLine("====================================");

            HashTableDouble<string> hashTableDoubleHash = new HashTableDouble<string>(tablesize);

            hashTableDoubleHash.DoubleHashingInsert("234", "354");
            hashTableDoubleHash.DoubleHashingInsert("134", "352");
            hashTableDoubleHash.DoubleHashingInsert("133", "133");
            hashTableDoubleHash.DoubleHashingInsert("456", "456");
            hashTableDoubleHash.Show();
            hashTableDoubleHash.DoubleHashingDelete("234");
            hashTableDoubleHash.DoubleHashingSearch("133");
            hashTableDoubleHash.Show();


            Console.ReadKey();


        }
    }
}